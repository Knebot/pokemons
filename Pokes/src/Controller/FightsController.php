<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Fights Controller
 *
 * @property \App\Model\Table\FightsTable $Fights
 *
 * @method \App\Model\Entity\Fight[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FightsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs'],
        ];
        $fights = $this->paginate($this->Fights);

        $this->set(compact('fights'));
    }

    /**
     * View method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fight = $this->Fights->get($id, [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs'],
        ]);

        $this->set('fight', $fight);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fight = $this->Fights->newEntity();
        if ($this->request->is('post')) {
            $fight = $this->Fights->patchEntity($fight, $this->request->getData());
            if ($this->Fights->save($fight)) {
                $this->Flash->success(__('The fight has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The fight could not be saved. Please, try again.'));
        }
        $firstDresseurs = $this->Fights->FirstDresseurs->find('list', ['limit' => 200]);
        $secondDresseurs = $this->Fights->SecondDresseurs->find('list', ['limit' => 200]);
        $winnerDresseurs = $this->Fights->WinnerDresseurs->find('list', ['limit' => 200]);
        $this->set(compact('fight', 'firstDresseurs', 'secondDresseurs', 'winnerDresseurs'));
    }
    public function getPokeID($dresseur_id)
    {
        $table = TableRegistry::getTableLocator()->get('dresseurs_pokes');
        $query1 = $table
        ->find()
        ->select(['poke_id'])
        ->where(['dresseur_id' => $dresseur_id])
        ->where(['favorite' => true]);
        $pokemon = 0;
        foreach ($query1 as $test) {
            $pokemon = $test->poke_id; 
        }
        return $pokemon;
    }
    public function getCaracPoke($poke_id)
    {
        $table = TableRegistry::getTableLocator()->get('pokes');
        $query1 = $table
        ->find()
        ->select(['name'])
        ->select(['health'])
        ->select(['defense'])
        ->select(['attack'])
        ->where(['pokedex_number' => $poke_id]);
        foreach ($query1 as $test) {
            return ['name' => $test->name, 'health' => $test->health, 'attack' => $test->attack, 'defense' => $test->defense];
        }
    }
    public function fighting($formDat)
    {
        $poke1 = $this->getPokeID($formDat['first_dresseur_id']);
        $poke2 = $this->getPokeID($formDat['second_dresseur_id']);

        $caracs1 = $this->getCaracPoke($poke1);
        $caracs2 = $this->getCaracPoke($poke2);

        $hp_poke1 = $caracs1['health'];
        $hp_poke2 = $caracs2['health'];
        $vainqueur = null;
        $poke_vainqueur = null;
        $adversaire = null;
        $hp = 0;
        while($vainqueur == null)
        {
            $hp_poke2 -= (((2.4) * $caracs1['attack'] * 40)/($caracs2['defense'] * 50) + 2);
            if($hp_poke2 <= 0)
            {
                $vainqueur = $formDat['first_dresseur_id'];
                $poke_vainqueur = $caracs1['name'];
                $adversaire = $caracs2['name'];
                $hp = $hp_poke1;
            }
            else
                $hp_poke1 -= (((2.4) * $caracs2['attack'] * 40)/($caracs1['defense'] * 50) + 2);
            if($hp_poke1 <= 0)
            {
                $vainqueur = $formDat['second_dresseur_id'];
                $hp = $hp_poke2;
                $poke_vainqueur = $caracs2['name'];
                $adversaire = $caracs1['name'];
            }
        }
        return ['vainqueur' => $vainqueur, 'pokemon_adv' => $adversaire, 'pokemon_vainqueur' => $poke_vainqueur,'hp' => $hp];
    }

  
    /**
     * Delete method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fight = $this->Fights->get($id);
        if ($this->Fights->delete($fight)) {
            $this->Flash->success(__('The fight has been deleted.'));
        } else {
            $this->Flash->error(__('The fight could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
